package application;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import model.dao.DaoFactory;
import model.dao.SellerDao;
import model.entities.Department;
import model.entities.Seller;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		SellerDao sellerDao = DaoFactory.CreateSellerDao();
		//DepartmentDao departmentDao = DaoFactory.CreateDepartmentDao();
		
		System.out.println("----- TEST 1: Seller findById -----");
		Seller seller = sellerDao.findById(9);
		
		System.out.println(seller);
		
		System.out.println("\n----- TEST 2: Seller findByDepartment  -----");
		Department dep = new Department(7, null);
		List<Seller> listSeller = sellerDao.findByDepartment(dep);
		
		for (Seller obj : listSeller) {
			System.out.println(obj);			
		}
		
		System.out.println("\n----- TEST 3: Seller findAll  -----");		
		List<Seller> listSeller2 = sellerDao.findAll();
		
		for (Seller obj : listSeller2) {
			System.out.println(obj);			
		}
		
		System.out.println("\n----- TEST 4: Seller insert  -----");		
		Seller seller1 = new Seller(null, "Zezim", "zezim@gmail.com", new Date(), 10000.0, dep);
		sellerDao.insert(seller1);
		
		System.out.println("Inserted! New id=" + seller1.getId());	
		
		System.out.println("\n----- TEST 5: Seller update  -----");		
		seller = sellerDao.findById(17);		
		seller.setName("Jo�ozim");
		seller.setBaseSalary(6500.0);		
		sellerDao.update(seller);
		
		System.out.println("Update completed!");	
		
		System.out.println("\n----- TEST 6: Seller delete  -----");	
		System.out.println("Enter id for delete test:");
		int id = sc.nextInt();
		sellerDao.deleteById(id);
		System.out.println("Delete completed");
		sc.close();
	}

}
