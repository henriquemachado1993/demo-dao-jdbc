package application;

import java.util.List;
import java.util.Scanner;

import model.dao.DaoFactory;
import model.dao.DepartmentDao;
import model.entities.Department;

public class Program2 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		DepartmentDao departmentDao = DaoFactory.CreateDepartmentDao();
		
		System.out.println("----- TEST 1: Department findById -----");
		Department department = departmentDao.findById(5);
		
		System.out.println(department);
		
		System.out.println("\n----- TEST 3: Department findAll  -----");		
		List<Department> listDepartment2 = departmentDao.findAll();
		
		for (Department obj : listDepartment2) {
			System.out.println(obj);			
		}
		
		System.out.println("\n----- TEST 4: Department insert  -----");		
		Department department1 = new Department(null, "Util");
		departmentDao.insert(department1);
		
		System.out.println("Inserted! New id=" + department1.getId());	
		
		System.out.println("\n----- TEST 5: Department update  -----");		
		department = departmentDao.findById(6);		
		department.setName("Eletric house");
		
		departmentDao.update(department);
		
		System.out.println("Update completed!");	
		
		System.out.println("\n----- TEST 6: Department delete  -----");	
		System.out.println("Enter id for delete test:");
		int id = sc.nextInt();
		departmentDao.deleteById(id);
		System.out.println("Delete completed");
		sc.close();
	}
}
